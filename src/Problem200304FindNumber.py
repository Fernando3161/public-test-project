'''
Created on 04.03.2020

@author: vaca-admin
'''
data = [11, 13, 24, 1, 4, 6, 7]

def separate(data):
    mid = int(len(data)/2)
    first=data[0:mid]
    second=data[mid:]
    print(first, second)
    return first, second

def binary_search(list, k,n=0):
    first, second = separate(list)
    if k <= first[-1]:
        second = first
    elif k > list[-1]:
        return -1
    if k == second[0]:
        return second[0], "found"
    elif k == second[-1]:
        return second[-1], "found"
    else: 
        return binary_search(second,k,n+1) 

def look_rotated_list(list,k):
    first, second = separate(list)
    if first[0]>first[-1]:
        look_rotated_list(first, k)
    if second[0]>second[-1]:
        look_rotated_list(second, k)
    if first[0]<first[-1] and k>=first[0] and k<=first[-1]:
        val=binary_search(first, k)
        if val!=-1:
            return val
    if second[0]<second[-1] and k>=second[0] and k<=second[-1]: 
        val=binary_search(second, k)
        if val!=-1:
            return val
    
            
    
if __name__ == '__main__':
    data = [(x+2)%100 for x in range (165,225)]
    print(data)
    print(look_rotated_list(data, 86))